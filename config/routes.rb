Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'projects#index'
  resources :projects, only: [:create, :destroy, :index] do
    resources :todos, only: [:index, :create, :destroy, :update, :edit]
  end

  resources :tags, only: [:index, :create, :destroy]
end

class CreateTodoTagRelationships < ActiveRecord::Migration[6.0]
  def change
    create_table :todo_tag_relationships do |t|
      t.integer :todo_id
      t.integer :tag_id

      t.timestamps
    end

    add_index :todo_tag_relationships, :todo_id
    add_index :todo_tag_relationships, :tag_id
    add_index :todo_tag_relationships, [:todo_id, :tag_id], unique: true
  end
end

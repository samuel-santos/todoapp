# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

5.times do |n|
  Project.create!(name: "Project #{n}")
end

5.times do |n|
  Tag.create!(name: "Tag #{n}")
end

todo_id = 0
Project.all.each do |p|
  11.times do |n|
    todo_id += 1
    content = "Todo #{n + 1}"
    completed = n.even?
    p.todos.create!(content: content, completed: completed, deadline: n.days.from_now)

    TodoTagRelationship.create!(todo_id: todo_id, tag_id: ((n + 1) % 5 + 1))
  end
end
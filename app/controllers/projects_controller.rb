class ProjectsController < ApplicationController

  def index
    @projects = Project.all.paginate(page: params[:page])
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    @projects = []
    if @project.save
      redirect_to root_path
    else
      render 'index'
    end
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    redirect_to root_path
  end

  private
  def project_params
    params
        .require(:project)
        .permit(:name)
  end
end

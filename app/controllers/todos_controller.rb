class TodosController < ApplicationController

  before_action :get_project, only: [:index, :edit, :update, :destroy, :create]

  def index
    @all_tags = Tag.all
    @todo_tags = []

    @all_todos = @project.todos.includes(:tags)
    @todos = @all_todos.todos_filter(@all_todos, params['filter']).paginate(page: params[:page], per_page: 20)
    @todo = Todo.new
  end

  def edit
    @all_tags = Tag.all

    @todo = @project.todos.find(params[:id])
    tag_ids = @todo.tags.ids
    @todo_tags = tag_ids.nil? ? [] : tag_ids
  end

  def create
    @todo = @project.todos.new(todo_params)
    @todos = []
    if @todo.save
      redirect_to project_todos_path
    else
      render 'todos/index'
    end

    @todo.tags = get_tags_from_params # posso mover isto lá para cima e fazer apenas 1 save?
    @todo.save
  end

  def update
    @todo = @project.todos.find(params[:id])
    @todos = []
    tag_ids = @todo.tags.ids
    @todo_tags = tag_ids.nil? ? [] : tag_ids

    p = todo_params
    c = nil
    c = p[:completed] unless p[:completed].nil?
    if @todo.update_attributes(
      content: (
        if p[:content].nil?
          @todo.content
        else
          p[:content]
        end
      ),
      completed: (if !c.nil?
                    c
                  else
                    @todo.completed
                  end),
      deadline: (
        if p[:deadline].nil?
          @todo.deadline
        else
          p[:deadline]
        end
      )
    )
      redirectTo(project_todos_path)
    else
      render 'todos/edit'
    end

    # caso em que se faz update ao status do to do e nao se enviam as tags (nao queremos apagar as tags que lá estavam)
    @todo.tags = get_tags_from_params unless c && params[:tags].nil?
    @todo.save # refatorizar para fazer apenas 1 save?
  end

  def destroy
    @todo = @project.todos.find(params[:id])
    @todo.destroy
    redirect_to project_todos_path
  end

  private

  def redirectTo(path)
    respond_to do |format|
      format.html { redirect_to path }
      format.js
    end
  end

  def get_tags_from_params
    tags = []
    params[:tags]&.each do |t|
      t_id = Integer(t[0]) rescue nil
      tags << Tag.find(t_id) if t_id && t[1] == '1'
    end
    
    return tags
  end

  def get_project
    @project = Project.find(params[:project_id])
  end

  def todo_params
    params
      .require(:todo)
      .permit(:content, :completed, :deadline)
  end
end

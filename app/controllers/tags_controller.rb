class TagsController < ApplicationController

  def index
    @tag = Tag.new
    @tags = Tag.all.paginate(page: params[:page])
  end

  def create
    @tag = Tag.new(tag_params)
    @tags = []
    if @tag.save
      redirect_to tags_path
    else
      render 'tags/index'
    end
  end

  def destroy
    @tag = Tag.find(params[:id])
    @tag.destroy
    redirect_to tags_path
  end

  private
  def tag_params
    params
        .require(:tag)
        .permit(:name)
  end
end

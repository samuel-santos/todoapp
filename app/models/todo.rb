class Todo < ApplicationRecord
  belongs_to :project
  has_many :todo_tag_relationships, class_name: 'TodoTagRelationship',
                                    foreign_key: 'todo_id',
                                    dependent: :destroy

  has_many :tags, through: :todo_tag_relationships, source: :tag

  validates :content, presence: true, allow_nil: true
  validates_inclusion_of :completed, in: [true, false], allow_nil: true
  validates :deadline, presence: true, allow_nil: true

  def self.todos_filter(todos, filter)
    begin
      if filter.keys.size == 2 && (filter['date_to'][0].blank? && filter['date_to'][0].blank?)
        return all
      end
    rescue StandardError
      return all
    end

    filter_tags = []
    filter_completed = []

    filter.each do |param|
      filter_field_key = param[0]
      filter_field_value = param[1]
      if filter_field_key.eql?('tags')
        filter_field_value.each do |tag_pair|
          tag_id = tag_pair[0]
          filter_tags << tag_id if tag_pair[1].eql?('on')
        end
      elsif filter_field_key.eql?('completed')
        filter_field_value.each do |completed_pair|
          filter_completed << true if completed_pair[0].eql?('done') && completed_pair[1].eql?('on')
          filter_completed << false if completed_pair[0].eql?('todo') && completed_pair[1].eql?('on')
        end
      end
    end

    todos = todos.where('completed IN (?)', filter_completed) if filter_completed.size.positive?
    todos = todos.where('deadline >= (?)', filter['date_from']) unless filter['date_from'][0].blank?
    todos = todos.where('deadline <= (?)', filter['date_to']) unless filter['date_to'][0].blank?
    tags = TodoTagRelationship.where('tag_id in (?)', filter_tags).select('todo_id')
    todos = todos.where('id in (?)', tags)
    return todos
  end
end

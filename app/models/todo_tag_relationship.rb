class TodoTagRelationship < ApplicationRecord
  belongs_to :todo, class_name: 'Todo'
  belongs_to :tag, class_name: 'Tag'

  validates :todo_id, presence: true
  validates :tag_id, presence: true
end

class Tag < ApplicationRecord
  validates :name, presence: true

  has_many :todo_tag_relationships, class_name: 'TodoTagRelationship',
                                    foreign_key: 'tag_id',
                                    dependent: :destroy

  has_many :todos, through: :todo_tag_relationships, source: :todo
end

class Project < ApplicationRecord
  validates :name, presence: true

  has_many :todos, dependent: :destroy

end
